#!/bin/bash
#
## on se place dans le repertoire ou sont les sauvegardes
#
cd /backups/

read utilisateur
read motdepasse
read nombase

for file in /backups/
do
    if [[ -f $file ]]; then
        ls -t -1 /backups/
    fi
done

#récupération nom fichier
echo "Entrez le nom du fichier a restaurer avec la date sans extensions"
read nomfichier

tar xvjf ${nomfichier}.tar.bz2

mysql -h localhost -u ${utilisateur} -p ${motdepasse}  ${nombase} < ${nomfichier}.sql

rm ${nomfichier}.sql







