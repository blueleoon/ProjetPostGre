#!/bin/bash
#
## Supprime les sauvegardes vieilles de plus de x jours
#

until [[ ${nombre} =~ ^[0-9]+$ ]]; do
	echo "Supprimer les sauvegardes vieilles de plus de x jours"
	echo "Saisir x"
	read nombre
	done


find /backups/ -type f -mtime +${nombre} -delete
find /backups/ -type d -mtime +${nombre} -delete